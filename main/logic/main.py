from aiohttp import web
from routers import setup_routers
from settings import config

app = web.Application()
setup_routers(app)
app['config'] = config
web.run_app(app)
