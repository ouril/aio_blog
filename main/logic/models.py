from peewee import *
import datetime
from .settings import config

db_info = config['postgres']

db = PostgresqlDatabase(
    db_info["database"],
    user=db_info["user"],
    password=db_info["password"],
    host=db_info["host"],
    port=db_info["port"]

)


class BaseModel(Model):
    id = BigIntegerField(primary_key=True)
    create_time = DateTimeField(default=datetime.datetime.now())

    class Meta:
        database = db


class User(BaseModel):
    name = CharField(max_length=128)
    email = CharField()
    status = BooleanField(default=True)
    age = IntegerField()


class Post(BaseModel):
    title = CharField(max_length=512)
    body = TextField()
    user = ForeignKeyField(User, on_delete='CASCADE')


def create_tables():
    with db:
        db.create_tables([User, Post])

