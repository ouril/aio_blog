from .settings import config
import peewee_async


def init_pg():
    conf = config['postgres']
    database = peewee_async.PostgresqlDatabase(**conf)
    objects = peewee_async.Manager(database)
    database.set_allow_sync(False)
    return objects